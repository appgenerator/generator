@extends('layouts.view')


@section('view')

	<div class="row">
		<h1 class="col-md-12">{{ trans($resource.'.index') }}</h1>
	</div>

	@if(isset($tip_messages['index']))
		<div class="alert alert-info">{{ trans($tip_messages['index']) }}</div>
	@endif

	@yield('list_top')
	{{ Form::open(['url' => module_route($resource.'.store'), 'files' => true]) }}
	<table class="table data-table">
		<thead>
		<tr>
			@foreach($list_fields as $field)
				<th>{{ trans($resource.'.'.$field) }}</th>
			@endforeach
			<th>
				<a href="{{ module_route($resource.'.create') }}" class="btn btn-primary">{{ trans($resource.'.create') }}</a>
				<!-- Button trigger modal -->
				<button type="button" class="btn btn-info pull-right" data-toggle="modal" data-target="#csvHelp">
				  <i class="fa fa-question-circle"></i> CSV Help
				</button>
				<!-- Modal -->
				<div class="modal fade" id="csvHelp" tabindex="-1" role="dialog" aria-labelledby="csvHelpLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
				        <h4 class="modal-title" id="csvHelpLabel">{{ trans('csv.index') }}</h4>
				      </div>
				      <div class="modal-body">

						<h3>Met een CSV bestand kunt u gemakkelijk meerdere items direct importeren in een feature.</h3>

						Hieronder staan voorbeeld bestanden van een CSV bestand van een aantal features.
						Het is van uiterst belang dat deze CSV's correct worden overgenomen.<br><br>

						<h4>Download hier het voorbeeld bestand van de feature: Locaties</h4>
						<a href="/downloads/Locaties-CSV.csv" class="btn btn-info pull-left">Download Locaties voorbeeld bestand</a><br><br>

						<h4>Download hier het voorbeeld bestand van de feature: Agenda</h4>
						<a href="/downloads/Agenda-Items-CSV.csv" class="btn btn-info pull-left">Download Agenda voorbeeld bestand</a><br>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-primary" data-dismiss="modal">Sluiten</button>
				      </div>
				    </div>
				  </div>
				</div>

				<span class="btn btn-default btn-file">
					CSV Bestand kiezen <input type="file" onchange="this.form.submit()" class="csv_file_upload" name="csv">
				</span>
				@if($has_settings)
					<a href="{{ module_route('settings.index') }}" class="btn btn-info">
						{{ trans($resource.'.settings') }}
					</a>
				@endif
			</th>
			<th>
				<center><span class="select-all">X</span></center>
			</th>
		</tr>
		</thead>
		<tbody>
		@foreach($items as  $item)

			<tr>
				@foreach($list_fields as $i=>$field)

						<td>
							{{ $item->{$field} }}
						</td>

				@endforeach

				<td class="text-right">
					@if($has_children)
						<a href="{{ module_route($resource.'.show',[$item->id]) }}" class="btn btn-default">{{ trans($resource.'.children') }}</a>
					@endif


					<a href="{{ module_route($resource.'.edit',[$item->id]) }}" class="btn btn-primary">
						{{ trans($resource.'.edit') }}
					</a>
				</td>
				<td>
					<center>
						<input type="checkbox" name="mass_delete[]" value="{{$item->id}}">
					</center>
				</td>
			</tr>

		@endforeach
		</tbody>
	</table>


	@if(count($items))
		<br>
		<div class="row">
			<div class="col-md-12">
				<input type="submit" class="btn btn-danger pull-right" id="bulk_delete" name="mass_delete_submit" value="Geselecteerde items verwijderen!"></td>
			</div>
		</div>
	@endif

	{{ Form::close() }}
	@yield('list_bottom')

@stop
@section('scripts')
	<script type="text/javascript">
	// Listen for click on toggle checkbox
	$('.select-all').click(function(event) {
		// Iterate each checkbox
		$(':checkbox').each(function() {
		    this.checked = true;
		});
	});
	</script>
@endsection