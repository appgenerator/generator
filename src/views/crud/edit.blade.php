@extends('layouts.view')


@section('view')

	<a href="{{ module_route($resource.'.index') }}" class="btn btn-default">{{ trans('forms.back') }}</a>

	<h1>{{ trans($resource.'.edit') }}</h1>

	{{ Form::open(['url' => module_route($resource.'.update',[$item->id]),'method' => 'PUT']) }}

	@yield('form_top')



	@foreach($form_fields as $name=>$macro)

		{{ Form::{$macro}($resource,$name,$item, (in_array($name, $rules) ? null : ['optional' => 1] ) ) }}
	@endforeach

	@yield('form_bottom')

	{{ Form::btn($resource.'.update','primary pull-right') }}

	{{ Form::close() }}

@stop
