@extends('layouts.view')


@section('view')

	<a href="{{ module_route($resource.'.index') }}" class="btn btn-default">{{ trans('forms.back') }}</a>
	<h1>{{ trans($resource.'.create') }}</h1>

	{{ Form::open(['url' => module_route($resource.'.store')]) }}

	@yield('form_top')

	@foreach($form_fields as $name=>$macro)
		{{ Form::{$macro}($resource,$name, null, (in_array($name, $rules) ? null : ['optional' => 1] ) ) }}
	@endforeach

	@yield('form_bottom')

	{{ Form::btn($resource.'.store','primary pull-right store-button') }}

	@foreach(Input::all() as $name=>$value)

	{{ Form::field($resource,$name,null, ['value' => $value, 'type' => 'hidden']) }}

	@endforeach

	{{ Form::close() }}
@stop
