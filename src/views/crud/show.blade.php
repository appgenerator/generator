@extends('layouts.view')


@section('view')


	<a href="{{ module_route($resource.'.index') }}" class="btn btn-default">{{ trans('forms.back') }}</a>

	<h1>{{ trans($child_resource.'.index') }}</h1>


	<table class="table">
		<tr>
			@foreach($list_fields as $field)
				<th>{{ trans($child_resource.'.'.$field) }}</th>
			@endforeach
			<th>
				<a href="{{ module_route($child_resource.'.create') }}?{{ $child_foreign }}={{ $parent->id }}" class="btn btn-primary">{{ trans($child_resource.'.create') }}</a>
			</th>
		</tr>

		@foreach($items as  $item)

			<tr>
				@foreach($list_fields as $i=>$field)

					@if($i == 0 && $has_children)
						<td>
							<a href="{{ module_route($child_resource.'.show',[$item->id]) }}">{{ $item->{$field} }}</a>
						</td>
					@else
						<td>{{ $item->{$field} }}</td>
					@endif
				@endforeach

				<td class="text-right">
					<a href="{{ module_route($child_resource.'.edit',[$item->id]) }}" class="btn btn-primary">
						{{ trans($child_resource.'.edit') }}
					</a>
					{{ Form::deletebtn(module_route($child_resource.'.destroy',[$item->id]),$child_resource.'.delete') }}
				</td>
			</tr>

		@endforeach
	</table>



@stop
