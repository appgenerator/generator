@extends('layouts.view')


@section('view')

	<a href="{{ module_route($resource.'.index') }}" class="btn btn-default">
		{{ trans('forms.back') }}
	</a>

	<h1>{{ trans($resource.'.settings') }}</h1>
	
	{{ Form::open(['url' => module_route('settings.save')]) }}

	@foreach($fields as $field=>$macro)

	{{ Form::{$macro}($resource,$field,$setting) }}

	@endforeach

	{{ Form::btn('forms.save','primary pull-right') }}

	{{ Form::close(); }}
@stop


