<?php

namespace Appgenerator\Generator\Module;

use Monga;

class Storage
{
	/**
	 * App id
	 * @var int
	 */
	private $appId;
	/**
	 * View id
	 * @var int
	 */
	public $viewId;
	/**
	 * Mongo connection
	 * @var object
	 */
	private $connection;
	/**
	 * Database object
	 * @var object
	 */
	private $database;
	/**
	 * Collection object
	 * @var object
	 */
	private $collection;
	/**
	 * Constructor
	 * @param int $appId
	 * @param int $viewId
	 */
	public function __construct($appId, $viewId = 0)
	{
		$this->appId = $appId;
		$this->viewId = $viewId;

		// $this->connection = Monga::connection();
		// $this->database = $this->connection->database('appgenerator');
		// $this->collection = $this->database->collection('app_' . $appId . '_view_' . $viewId);
	}

	public function getViewId()
	{
		return md5('app' . $this->appId . '_view' . $this->viewId);
	}

	public function getViewData()
	{
		return [];
		return $this->findOne(['view_id' => $this->getViewId()]);
	}

	public function updateViewData($data)
	{
		return false;
		// check if record exists
		$record = $this->getViewData();
		if (count($record)) {
			foreach($data as $k => $v) {
				$record[$k] = $v;
			}
			// dd($record);
			return $this->collection->save($record);
		} else {
			$data['view_id'] = $this->getViewId();

			return $this->insert($data);
		}
	}

	public function __call($name, $args = null)
	{
		if (method_exists($this->collection, $name)) {
			if (null === $args) {
				return $this->collection->{$name}();
			} else {
				return call_user_func_array([$this->collection, $name], $args);
			}
		}
	}
}
