<?php


namespace Appgenerator\Generator\Module;

class Settings extends Builder
{


	protected $resource = 'settings';

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function index()
	{
		return \View::make('generator::settings.form',[
				'app' => $this->app,
				'resource' => $this->resource,
				'fields' => $this->viewSettings,
				'setting' => \ViewSetting::getObject($this->view->id)
			]);
	}	

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function save()
	{
		$settings = \ViewSetting::where('view_id','=',$this->view->id)->get();

		foreach($settings as $set){
			$set->delete();
		}

		foreach(\Input::get($this->resource) as $setting=>$value){
			\ViewSetting::create([
					'view_id' => $this->view->id,
					'setting' => $setting,
					'value' => $value
				]);
		}

		return \Redirect::back()->withMessage(trans('settings.saved'));
	}

}
