<?php

namespace Appgenerator\Generator\Module;

class Builder extends \BaseController
{

	/**
	 * File path to root (src/) of module
	 * @var string
	 */
	private $rootPath;

	/**
	 * containts the dtaabase record of the view
	 * @var array
	 **/
	public $view;

	/**
	 * Containts the database record of the app
	 * @var array
	 **/
	public $app;

	/**
	 * Contains the tip messages for every action
	 * @var array
	 **/
	protected $tipMessages = [];


	/**
	 * Retrieves the right module builder class
	 * @param  string $repositoryUrl
	 * @return object
	 */
	public static function get($repositoryUrl, $controller = 'Builder')
	{
		$namespace = '\\' . implode('\\', array_map(function($name) {
			return ucfirst(strtolower($name));
		}, explode('/', $repositoryUrl))) . '\\' . $controller;

		if (class_exists($namespace)) {
			return new $namespace;
		}

		return;
	}


	/**
	 * Constructor
	 */
	public function __construct()
	{
		// add module view namespace
		\View::addNamespace($this->namespace, $this->findRootPath() . '/views');
		\View::addNamespace('generator', $this->createRootPath(__DIR__) .'/views' );

		// call BaseController constructor
		parent::__construct();

		$this->setupLayout();

		if(defined('CURRENT_VIEW_ID')){

			$view = \Builder\View::find(CURRENT_VIEW_ID);
		    $this->view = $view;
		    $this->app = \Builder\App::find($view->app_id);
		    $this->getStorage();

		    \View::share('app',$this->app);
		    \View::share('view',$this->view);


		}

	}

	/**
	 * Loads the storage for the module
	 * @return void
	 * @author Stef Wijnberg
	 **/
	public function getStorage()
	{
		$this->storage = new Storage($this->app->id, $this->view->id);

		return $this->storage;
	}

	/**
	 * Returns the namespaced view name
	 * @return string
	 */
	public function getView($appId, $viewId)
	{
		$view = $this->namespace . '::' . $this->view;

		$app = \Builder\App::find($appId);
		$view = \Builder\View::find($viewId);

		return View::Make($view,[
				'app' => $app,
				'view' => $view
			]);
	}

	public function getData()
	{
		$data = [];
		foreach (array_keys($this->rules) as $name) {
			$data[$name] = \Input::get($name);
		}

		return $data;
	}


	/**
	 * Gets the root path of the current module
	 * @return void
	 * @author Stef Wijnberg
	 **/
	function createRootPath($path)
	{
		$pathFragments = explode(DIRECTORY_SEPARATOR, $path);

		while($pathFragments) {
			if ('src' != end($pathFragments)) {
				array_pop($pathFragments);
			} else {
				break;
			}
		}

		return implode('/', $pathFragments);
	}

	/**
	 * Find root path based on path property
	 * @return string
	 */
	private function findRootPath()
	{
		if (!$this->rootPath) {

			$this->rootPath = $this->createRootPath($this->path);
		}


		return $this->rootPath;
	}
	/**
	 * Validates data
	 * @return object
	 */
	public function validate()
	{
		$this->validator = \Validator::make(\Input::all(), $this->rules);

		return $this->validator->passes();
	}
	/**
	 * Get errors through validator
	 * @return object
	 */
	public function errors()
	{
		return $this->validator->errors();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function getModel($name = '')
	{

		if($name == ''){
			$name = $this->model;
		}
		$modelName = 'Appgenerator\\'.ucfirst($this->namespace).'\\'. $name;

		return new $modelName;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author
	 **/
	function getIndexRoute($item)
	{

		$url = module_route($this->resource.'.index');

		// redirect back to the show page for the (parent) resource
		if(isset($this->parentResource) && isset($item->{$this->parentForeign})){
			$url = module_route($this->parentResource.'.show',[
					$item{$this->parentForeign}
				]);
		}

		return $url;
	}


    /**
	 * The default index view for a builder controller
	 * @return void
	 * @author
	 **/
	function index(){

		$model = $this->getModel();
		$items = $model::where('view_id','=',$this->view->id)->get();


		return $this->makeView('index',[
			'items' => $items
		]);
	}

	/**
	 * The default store method for a Builder Controller
	 * @return void
	 * @author Stef Wijnberg
	 **/
	public function store(){
		if(\Input::get('mass_delete_submit') && is_array(\Input::get('mass_delete')) && !empty(\Input::get('mass_delete'))) {
			$toDelete = \Input::get('mass_delete');

			# Get the model.
			$model = $this->getModel();

			foreach($toDelete as $id) {
				$item = $model::find($id);

				if($item->view_id != $this->view->id){
					\App::abort(403,'No access');
				}

				$model::destroy($id);
			}


			$url = $this->getIndexRoute($model);

			return \Redirect::to($url)
				->withMessage(trans('app.destroyed-bulk'));
		}

		if(\Input::hasFile('csv')) {
			# Hier het bestand ophalen en uitlezen.
			$model = $this->getModel();

			\Excel::load(\Input::file('csv'), function($reader) {
				$failed = 0;

				$results = $reader->get()->toArray();
				$count = count($results);

				$model = $this->getModel();

				foreach($results as $result) {
					# View ID ophalen
					$columns = \Schema::getColumnListing($model->getTable());

					foreach($result as $key => $name) {
						if(!in_array($key, $columns)) {
							unset($result);
						}
					}

					$result['view_id'] = $this->view->id;

					# Rij valideren
					if(is_array($result) && !empty($result)) {
						$validator = \Validator::make($result, $model::$rules);
					}

					# Wanneer goed DATABASE insert.
					if($validator->passes()) {
						try {
							$item = $model::create($result);
						} catch (Exception $e) {
							continue;
						}
					}
				}
			});

			# Gebruiker terug sturen naar de index en een status bericht meegeven
			$url = $this->getIndexRoute($model);

			return \Redirect::to($url)
				->withMessage(trans('app.importedcsv'));
		} else {
			// create the model and validate
			$model 		= $this->getModel();
		 	$data 		= \Input::get($this->resource);

			$validator 	= \Validator::make($data, $model::$rules);

			if(is_array(trans($this->resource))){
				$validator->setAttributeNames(trans($this->resource));
			}
			// check validation
			if($validator->fails()){
				return \Redirect::back()->withInput()->withErrors($validator);
			} else {


				// no errors, create and redirect back to resource index
				$data['view_id'] = $this->view->id;
				$item = $model::create($data);

				$url = $this->getIndexRoute($item);

				return \Redirect::to($url)
					->withMessage(trans($this->resource.'.created'));
			}
		}
	}


	/**
	 * The default update method for a builder controller
	 * @return void
	 * @author Stef Wijnberg
	 **/
	public function update($id){

		// create the model and validate
		$model 		= $this->getModel();
	 	$data 		= \Input::get($this->resource);
		$validator 	= \Validator::make($data, $model::$rules);

		if(is_array(trans($this->resource))){
			$validator->setAttributeNames(trans($this->resource));
		}


		$model 		= $this->getModel();
		$item 		= $model::find($id);

		if($item->view_id != $this->view->id){
			\App::abort(403,'No access');
		}

		// check validation
		if($validator->fails()){
			return \Redirect::back()->withInput()->withErrors($validator);
		}else{


			// no errors, create and redirect back to resource index
			$data['view_id'] = $this->view->id;

			$item->update($data);

			$url = $this->getIndexRoute($item);

			return \Redirect::to($url)
				->withMessage(trans($this->resource.'.updated'));
		}

	}


	/**
	 * The default create function for a Builder Controller
	 * @return void
	 * @author Stef Wijnberg
	 **/
	function create()
	{
		return $this->makeView('create');
	}

	/**
	 * The default destroy action for a builder controller
	 * @return void
	 * @author Stef Wijnberg
	 **/
	function destroy($id)
	{
		$model = $this->getModel();
		$item = $model::find($id);
		if($item->view_id != $this->view->id){
			\App::abort(403,'No access');
		}

		$item->delete();

		$url = $this->getIndexRoute($item);

		return \Redirect::to($url)
			->withMessage(trans($this->resource.'.destroyed'));
	}


	/**
	 *
	 * @return void
	 * @author Stef Wijnberg
	 **/
	function edit($id){


		$model = $this->getModel();
		$item = $model::find($id);

		if($item->view_id != $this->view->id){
			\App::abort(403,'No access');
		}


		return $this->makeView('edit',[
			'item' => $item
		]);
	}

	/**
	 * Shows the children of the item that is being edited
	 * @return void
	 * @author Stef Wijnberg
	 **/
	function show($id)
	{
		$model = $this->getModel();

		$parent = $model::find($id);

		$childModel = $this->getModel($this->childModel);

		$items = $childModel::where($this->childForeign,'=',$id)->get();

		return $this->makeView('show',[
				'items' => $items,
				'parent' => $parent
			]);
	}


	/**
	 * Generates a view for the default builder
	 * @return void
	 * @author Stef Wijnberg
	 **/
	protected function makeView($type,$params = []){

		$viewPath = strtolower($this->namespace).'::'.$this->resource.'.'.$type;

		if(!\View::exists($viewPath)){
			$viewPath = 'generator::crud.'.$type;
		}

		$rules = [];
		$model 		= $this->getModel();

		if(isset($model::$rules)){

			$rules = array_keys($model::$rules);
		}



		$params['rules'] 		= $rules;
		$params['resource'] 	= $this->resource;
		$params['form_fields'] 	= $this->formFields;
		$params['list_fields'] 	= $this->listFields;
		$params['tip_messages'] = $this->tipMessages;

		if(isset($this->hasSettings)){
			$params['has_settings'] = $this->hasSettings;
		}else{
			$params['has_settings'] = false;
		}


		if(isset($this->childResource) ){

			$params['has_children'] 	= true;
			$params['child_resource'] 	= $this->childResource;
			$params['child_model'] 		= $this->childModel;
			$params['child_foreign'] 	= $this->childForeign;

			if($type == 'show'){
				$params['list_fields']		= $this->childList;
			}

		}else{
			$params['has_children'] = false;
		}




		return \View::make($viewPath,$params);
	}

}
