<?php

namespace Appgenerator\Generator\Module;

class OldBuilder extends \BaseController
{

	/**
	 * File path to root (src/) of module
	 * @var string
	 */
	private $rootPath;

	/**
	 * containts the dtaabase record of the view
	 * @var array
	 **/
	public $view;

	/**
	 * Containts the database record of the app
	 * @var array
	 **/
	public $app;



	/**
	 * Retrieves the right module builder class
	 * @param  string $repositoryUrl
	 * @return object
	 */
	public static function get($repositoryUrl, $controller = 'Builder')
	{
		$namespace = '\\' . implode('\\', array_map(function($name) {
			return ucfirst(strtolower($name));
		}, explode('/', $repositoryUrl))) . '\\' . $controller;

		if (class_exists($namespace)) {
			return new $namespace;
		}

		return;
	}


	/**
	 * Constructor
	 */
	public function __construct()
	{
		// add module view namespace
		\View::addNamespace($this->namespace, $this->findRootPath() . '/views');
		\View::addNamespace('generator', $this->createRootPath(__DIR__) .'/views' );

		// call BaseController constructor
		parent::__construct();

		$this->setupLayout();

		if(defined('CURRENT_VIEW_ID')){

			$view = \Builder\View::find(CURRENT_VIEW_ID);
		    $this->view = $view;
		    $this->app = \Builder\App::find($view->app_id);
		    $this->getStorage();

		    \View::share('app',$this->app);
		    \View::share('view',$this->view);


		}

	}

	/**
	 * Loads the storage for the module
	 * @return void
	 * @author Stef Wijnberg
	 **/
	public function getStorage()
	{
		$this->storage = new Storage($this->app->id, $this->view->id);

		return $this->storage;
	}

	/**
	 * Returns the namespaced view name
	 * @return string
	 */
	public function getView($appId, $viewId)
	{
		$view = $this->namespace . '::' . $this->view;

		$app = \Builder\App::find($appId);
		$view = \Builder\View::find($viewId);

		return View::Make($view,[
				'app' => $app,
				'view' => $view
			]);
	}

	public function getData()
	{
		$data = [];
		foreach (array_keys($this->rules) as $name) {
			$data[$name] = \Input::get($name);
		}

		return $data;
	}


	/**
	 * Gets the root path of the current module
	 * @return void
	 * @author Stef Wijnberg
	 **/
	function createRootPath($path)
	{
		$pathFragments = explode(DIRECTORY_SEPARATOR, $path);

		while($pathFragments) {
			if ('src' != end($pathFragments)) {
				array_pop($pathFragments);
			} else {
				break;
			}
		}

		return implode('/', $pathFragments);
	}

	/**
	 * Find root path based on path property
	 * @return string
	 */
	private function findRootPath()
	{
		if (!$this->rootPath) {
			
			$this->rootPath = $this->createRootPath($this->path);
		}


		return $this->rootPath;
	}
	/**
	 * Validates data
	 * @return object
	 */
	public function validate()
	{
		$this->validator = \Validator::make(\Input::all(), $this->rules);

		return $this->validator->passes();
	}
	/**
	 * Get errors through validator
	 * @return object
	 */
	public function errors()
	{
		return $this->validator->errors();
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function getModel($name = '')
	{

		if($name == ''){
			$name = $this->model;
		}
		$modelName = 'Appgenerator\\'.ucfirst($this->namespace).'\\'. $name;

		return new $modelName;
	}

	/**
	 * undocumented function
	 *
	 * @return void
	 * @author 
	 **/
	function getIndexRoute()
	{

		$url = module_route($this->resource.'.index');
		
		// redirect back to the show page for the (parent) resource
		if(isset($this->parentResource) && isset($item->{$this->parentForeign})){
			$url = module_route($this->parentResource.'.show',[
					$data[$this->parentForeign]
				]);
		}

		return $url;
	}


    
}
