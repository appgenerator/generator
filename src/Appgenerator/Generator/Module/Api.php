<?php

namespace Appgenerator\Generator\Module;

class Api extends \Controller
{



    /**
     * Namespace of this module, used to distinguish modules
     * @var string
     */
    protected $namespace = 'module';

    /**
     * Path to module directory, used for autoloading files
     * @var string
     */
    protected $path = __DIR__;

    /**
     * Validation rules
     * @var array
     */
    protected $rules = [

    ];

	/**
	 * File path to root (src/) of module
	 * @var string
	 */
	private $rootPath;

	/**
	 * Contains the monga collection
	 * @var Monga\collection
	 **/
	public $storage = null;
	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		// add module view namespace
		\View::addNamespace($this->namespace, $this->findRootPath() . '/views');
	}

	/**
	 * Find root path based on path property
	 * @return string
	 */
	private function findRootPath()
	{
		if (!$this->rootPath) {
			$pathFragments = explode(DIRECTORY_SEPARATOR, $this->path);
			while($pathFragments) {
				if ('src' != end($pathFragments)) {
					array_pop($pathFragments);
				} else {
					break;
				}
			}

			$this->rootPath = implode('/', $pathFragments);
		}

		return $this->rootPath;
	}

	/**
	 * Retrieves the right module builder class
	 * @param  string $repositoryUrl 
	 * @return object                
	 */
	public static function get($repositoryUrl)
	{
		$namespace = '\\' . implode('\\', array_map(function($name) {
			return ucfirst(strtolower($name));
		}, explode('/', $repositoryUrl))) . '\\Api';

		if (class_exists($namespace)) {
			return new $namespace;
		}
		

		return;
	}

	public function show(){

		return $this->storage->getViewData();

	}
}


?>